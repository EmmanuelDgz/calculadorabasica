package com.emmanuel.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSuma = findViewById(R.id.btnSuma);
        Button btnResta = findViewById(R.id.btnResta);
        Button btnProduct = findViewById(R.id.btnProduct);
        Button btnDivision = findViewById(R.id.btnDivision);
        Button btnPower = findViewById(R.id.btnPower);
        Button btnPercent = findViewById(R.id.btnPercent);

        btnSuma.setOnClickListener(this);
        btnResta.setOnClickListener(this);
        btnProduct.setOnClickListener(this);
        btnDivision.setOnClickListener(this);
        btnPower.setOnClickListener(this);
        btnPercent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        TextView res = findViewById(R.id.labelResult);

        TextView numA = findViewById(R.id.numA);
        TextView numB = findViewById(R.id.numB);

        try {
            double a = Double.parseDouble(numA.getText().toString());
            double b = Double.parseDouble(numB.getText().toString());
            double resultado = 0.0;

            switch (v.getId()) {
                case R.id.btnSuma:
                    resultado = a + b;
                    break;
                case R.id.btnResta:
                    resultado = a - b;
                    break;
                case R.id.btnProduct:
                    resultado = a * b;
                    break;
                case R.id.btnDivision:
                    resultado = a / b;
                    break;
                case R.id.btnPower:
                    resultado = Math.pow(a, b);
                    break;
                case R.id.btnPercent:
                    resultado = a * (b/100);
            }
            res.setText(""+resultado);
        } catch (Exception e) {
            res.setText("Los valores deben ser numéricos");
        }
    }
}
